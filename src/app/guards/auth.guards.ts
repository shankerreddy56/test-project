import {Injectable} from "@angular/core";
@Injectable({
    providedIn:"root"
})
export class authguard{
    canActivate():boolean{
        return confirm("do u want to enter into pageone component");

    }
    canDeactivate():boolean{
        return confirm("do u want to leave pagetwo");
    }
    canActivateChild():boolean{
        return confirm("do u want to enter into child three");
    }
};