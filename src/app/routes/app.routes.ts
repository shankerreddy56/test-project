import { Routes } from '@angular/router';
import { PageoneComponent } from '../components/pageone.component';
import { Component } from '@angular/core';
import { PagetwoComponent } from '../components/pagetwo.component';
import { PagethreeComponent } from '../components/pagethree.component';
import { ChildoneComponent } from '../components/childone.component';
import { ChildtwoComponent } from '../components/childtwo.component';
import { ChildthreeComponent } from '../components/childthree.component';
import { authguard } from '../guards/auth.guards';

export const approutes:Routes=[
    {path:"page_one",component:PageoneComponent,
    children:[{path:"child_one/:p_id/:p_name/:p_cost",component:ChildoneComponent}],
        canActivate:[authguard]},
    {path:"page_two",component:PagetwoComponent,
    children:[{path:"child_two",component:ChildtwoComponent}],
        canDeactivate:[authguard]},
    {path:"page_three",component:PagethreeComponent,
    children:[{path:"child_three",component:ChildthreeComponent}],
        canActivateChild:[authguard]}
];